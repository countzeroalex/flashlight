package com.sample.flashlight.application;

import android.content.Context;
import android.content.SharedPreferences;

import com.sample.flashlight.R;
import com.sample.flashlight.service.FlashLightService;

public class AppPreferences {

    private static final String PREF_NAME = "com.sample.flashlight.SHARED_PREFERENCES";

    private static final String FIRST_TIME = "com.sample.flashlight.FIRST_TIME";
    private static final String FLASH_LIGHT_MODE = "com.sample.flashlight.FLASH_LIGHT_MODE";
    private static final String POWER_ON = "com.sample.flashlight.POWER_ON";
    private static final String FLASH_LIGHT_SIDE = "com.sample.flashlight.FLASH_LIGHT_SIDE";
    private static final String SELECTED_FLASH_MODE = "com.sample.flashlight.SELECTED_FLASH_MODE";
    private static final String MORSE_MODE_MESSAGE = "com.sample.flashlight.MORSE_MODE_MESSAGE";
    private static final String CAMERA_HARDWARE_ENABLE = "com.sample.flashlight.CAMERA_HARDWARE_ENABLE";
    private static final String FIRST_WIDGET_CALL = "com.sample.flashlight.FIRST_WIDGET_CALL";

    private final SharedPreferences mPref;
    private static AppPreferences sInstance;

    private AppPreferences(Context context) {
        mPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static synchronized AppPreferences getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new AppPreferences(context);
        }
        return sInstance;
    }

    // region FIRST_TIME
    public void setFirstTime(boolean value) {
        mPref.edit().putBoolean(FIRST_TIME, value).apply();
    }

    public boolean isFirstTime() {
        return mPref.getBoolean(FIRST_TIME, true);
    }
    // endregion FIRST_TIME

    //region SELECTED FLASH MODE
    public String getSelectedFlashMode(){
        return mPref.getString(SELECTED_FLASH_MODE, FlashLightService.MODE_CONTINUOUS);
    }

    public void setSelectedFlashMode(String value){
        mPref.edit().putString(SELECTED_FLASH_MODE, value).apply();
    }
    //endregion

    // region FLASH_LIGHT_MODE
    public void setSelectedFlashModeViewId(int value) {
        mPref.edit().putInt(FLASH_LIGHT_MODE, value).apply();
    }

    public int getSelectedFlashModeViewId() {
        return mPref.getInt(FLASH_LIGHT_MODE, R.id.activity_main_radio_flashlight_btn);
    }
    // endregion FLASH_LIGHT_MODE

    // region POWER_ON
    public void setPowerOn(boolean value) {
        mPref.edit().putBoolean(POWER_ON, value).apply();
    }

    public boolean isPowerOn() {
        return mPref.getBoolean(POWER_ON, false);
    }
    // endregion POWER_ON

    // region FLASH_LIGHT_SIDE
    public void setLedCameraSelected(boolean value) {
        mPref.edit().putBoolean(FLASH_LIGHT_SIDE, value).apply();
    }

    public boolean isLedCameraSelected() {
        return mPref.getBoolean(FLASH_LIGHT_SIDE, true);
    }

    // endregion FLASH_LIGHT_SIDE

    //region MORSE MODE MESSAGE
    public String getMorseModeMessage(){
        return mPref.getString(MORSE_MODE_MESSAGE, "SOS");
    }

    public void setMorseModeMessage(String value){
        mPref.edit().putString(MORSE_MODE_MESSAGE, value).apply();
    }
    //endregion

    // region CAMERA_HARDWARE_ENABLE
    public void setCameraHardwareAvailable(boolean value) {
        if (!value) {
            setLedCameraSelected(false);
        }
        mPref.edit().putBoolean(CAMERA_HARDWARE_ENABLE, value).apply();
    }

    public boolean isCameraHardwareAvailable() {
        return mPref.getBoolean(CAMERA_HARDWARE_ENABLE, false);
    }
    // endregion CAMERA_HARDWARE_ENABLE

    //region WIDGET ENABLED
    public boolean isFirstWidgetCall(){
        return mPref.getBoolean(FIRST_WIDGET_CALL, true);
    }

    public void setFirstWidgetCall(boolean value){
        mPref.edit().putBoolean(FIRST_WIDGET_CALL, value).apply();
    }
    //endregion
}
