package com.sample.flashlight.service;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.view.View;
import android.widget.RemoteViews;

import com.sample.flashlight.R;
import com.sample.flashlight.activity.ScreenFlashActivity;
import com.sample.flashlight.application.AppPreferences;
import com.sample.flashlight.util.MorseCodeTick;
import com.sample.flashlight.util.MorseCodeUtil;
import com.sample.flashlight.util.NotificationUtil;
import com.sample.flashlight.widget.WidgetProvider;

import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("deprecation")
public class FlashLightService extends Service implements Camera.ErrorCallback{

    //action to toggle on/off no matter the selected mode
    public static final String FROM_WIDGET = "com.sample.flashlight.FROM_WIDGET";
    public static final String WIDGET_VISIBILITY = "com.sample.flashlight.WIDGET_VISIBILITY";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({
            MODE_OFF,
            MODE_STROBE,
            MODE_MORSE,
            MODE_CONTINUOUS,
            UPDATE_WIDGET
    })

    public @interface ServiceAction {}
    public static final String MODE_OFF = "off";
    public static final String MODE_STROBE = "strobe";
    public static final String MODE_MORSE = "morse";
    public static final String MODE_CONTINUOUS = "continuous";
    public static final String UPDATE_WIDGET = "updateWidget"; //sent from Activity to update widget, not to be confused with floating widget

    private Camera mCamera;
    private ScheduledExecutorService mMorseCodeTicksExecutor;

    private Camera.Parameters mStrobeParameters;
    private SurfaceTexture mSurfaceTexture;
    private Handler mHandler = new Handler();
    private boolean mIsStrobeOn = false;
    private int mWidgetVisibility = View.VISIBLE;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        AppPreferences appPreferences = AppPreferences.getInstance(getApplicationContext());

        boolean fromWidget = false;
        if(intent != null){
            fromWidget = intent.getBooleanExtra(FROM_WIDGET, false);
            if (intent.getExtras() != null) {
                Bundle extras = intent.getExtras();
                mWidgetVisibility = extras.getInt(WIDGET_VISIBILITY);
            }
        }

        String mode = "";
        if (intent != null && intent.getAction() != null) {
            mode = intent.getAction();
        } else if(appPreferences.isPowerOn()){
            mode = appPreferences.getSelectedFlashMode();
        }

        /**
         * If the device does not have a led light or the Screen Light option is
         * selected, release the camera and send the intent action to ScreenFlashActivity to handle it
         */
        if (!appPreferences.isCameraHardwareAvailable() || !appPreferences.isLedCameraSelected()) {
            if(mode.equals(UPDATE_WIDGET)){
                updateWidget(appPreferences.isPowerOn(), mWidgetVisibility);
            } else {
                try {
                    new StopCameraPreviewAsync().execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (!mode.isEmpty()) {
                    Intent screenFlashIntent = new Intent(this, ScreenFlashActivity.class);
                    screenFlashIntent.setAction(mode);
                    screenFlashIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    startActivity(screenFlashIntent);
                }
            }
        } else {
            try {
                if(mode.equals(UPDATE_WIDGET)) {
                    //called separately to avoid removing callbacks from camera or resetting it
                    updateWidget(AppPreferences.getInstance(getApplicationContext()).isPowerOn(), mWidgetVisibility);
                } else if (new CameraOpenAsync().execute().get()) {
                    Camera.Parameters parameters = mCamera.getParameters();

                    /**reset camera runnables**/
                    mHandler.removeCallbacks(mStrobeRunnable);

                    if(mMorseCodeTicksExecutor != null && !mMorseCodeTicksExecutor.isShutdown()) {
                        mMorseCodeTicksExecutor.shutdownNow();
                    }

                    /** prevent call from widget provider - prevents flash-actions when power is off ***/
                    if (!appPreferences.isPowerOn() && !fromWidget) {
                        mode = MODE_OFF;
                    }
                    switch (mode) {
                        case MODE_OFF:
                            new StopCameraPreviewAsync().execute();
                            appPreferences.setPowerOn(false);
                            break;
                        case MODE_CONTINUOUS:
                            turnFlashOn(parameters);
                            appPreferences.setPowerOn(true);
                            break;
                        case MODE_MORSE:
                            turnMorseCodeModeOn(AppPreferences.getInstance(getApplicationContext()).getMorseModeMessage());
                            appPreferences.setPowerOn(true);
                            break;
                        case MODE_STROBE:
                            turnStrobeFlashOn(parameters);
                            appPreferences.setPowerOn(true);
                            break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        new StopCameraPreviewAsync().execute();
        releaseCamera();
    }

    //region ##FLASH MODES
    private void turnMorseCodeModeOn(String message){
        ArrayList<MorseCodeTick> morseCodeTickArrayList = MorseCodeUtil.getMorseCodeSequence(message);

        if(morseCodeTickArrayList.size() == 0){
            return;
        }

        mMorseCodeTicksExecutor = Executors.newSingleThreadScheduledExecutor();
        long delay = 500;

        for(int i=0;i<morseCodeTickArrayList.size();i++){
            final MorseCodeTick morseCodeTick = morseCodeTickArrayList.get(i);

            mMorseCodeTicksExecutor.schedule(
                    new Runnable() {
                        @Override
                        public void run() {
                            if(morseCodeTick.isSignalOn()){
                                turnFlashOn(mCamera.getParameters());
                            } else {
                                turnFlashOff(mCamera.getParameters());
                            }
                        }
                    },
                    delay,
                    TimeUnit.MILLISECONDS);

            delay += morseCodeTick.getDuration();
        }
    }

    private void turnStrobeFlashOn(Camera.Parameters parameters) {
        mStrobeParameters = parameters;
        mHandler.postDelayed(mStrobeRunnable, 200);
    }

    private void turnFlashOn(Camera.Parameters parameters) {
        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        mCamera.setParameters(parameters);
    }

    private void turnFlashOff(Camera.Parameters parameters){
        try {
            //if flash is currently on in any mode, turn it off
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            mCamera.setParameters(parameters);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    Runnable mStrobeRunnable = new Runnable() {
        @Override
        public void run() {
            try {
                if (mIsStrobeOn) {
                    mStrobeParameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                } else {
                    mStrobeParameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                }

                mIsStrobeOn = !mIsStrobeOn;

                if (mCamera != null) {
                    mCamera.setParameters(mStrobeParameters);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                mHandler.postDelayed(this, 1000);
            }
        }
    };

    //endregion

    //region ##WIDGET
    private void updateWidget(boolean flashOn, int visibility){
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getApplicationContext());
        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.widget_layout);
        ComponentName thisWidget = new ComponentName(getApplication(), WidgetProvider.class);
        remoteViews.setImageViewResource(R.id.widget_enabled_light, flashOn ? R.drawable.widget_on :R.drawable.widget_off);
        remoteViews.setViewVisibility(R.id.widget_enabled_light, visibility);
        appWidgetManager.updateAppWidget(thisWidget, remoteViews);
    }
    //endregion

    //region ##Camera Methods
    private void releaseCamera(){
        if(mSurfaceTexture != null){
            mSurfaceTexture.release();
        }

        if(mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    private boolean safeCameraOpen() {
        boolean cameraOpened = false;

        try {
            releaseCamera();
            mCamera = Camera.open();
            setCameraSurface();
            mCamera.setErrorCallback(this);
            mCamera.startPreview();
            cameraOpened = (mCamera != null);
        } catch (Exception e) {
            mCamera = null;
            e.printStackTrace();
        }

        return cameraOpened;
    }

    private boolean setCameraSurface(){

        if(mCamera == null){
            return false;
        }

        try {
            mSurfaceTexture = new SurfaceTexture(0);
            mCamera.setPreviewTexture(mSurfaceTexture);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void onError(int error, Camera camera) {
        releaseCamera();
        NotificationUtil.createCameraErrorNotification(FlashLightService.this);
    }

    private class CameraOpenAsync extends AsyncTask<Void, Void, Boolean>{

        @Override
        protected Boolean doInBackground(Void... params) {
            return safeCameraOpen();
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
        }
    }

    private class StopCameraPreviewAsync extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... params) {
            releaseCamera();
            return null;
        }
    }
    //endregion
}
