package com.sample.flashlight.activity;

import android.Manifest;
import android.content.Intent;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.sample.flashlight.R;
import com.sample.flashlight.application.AppPreferences;
import com.sample.flashlight.service.FlashLightService;
import com.sample.flashlight.util.AlertUtil;
import com.sample.flashlight.util.HardwareUtil;
import com.sample.flashlight.util.MorseCodeUtil;
import com.sample.flashlight.util.NotificationUtil;
import com.sample.flashlight.util.PermissionUtil;

public class MainActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback,
        RadioGroup.OnCheckedChangeListener, View.OnLayoutChangeListener{

    private static final int PERMISSION_REQUEST_CAMERA = 130;
    private float mDensityFactor;

    private AppPreferences mAppPreferences;
    private RadioGroup mModeRadioGroup;
    private boolean mCameraSupported = false;
    private EditText mMorseMsgEditTxt;

    //boolean that saves power state when touch action starts
    private boolean mPowerOnActionDown = false;

    private MediaPlayer mMediaPlayer;

    //Parent ViewGroup used to handle touch events
    private ViewGroup mLinearLayout;

    //region ##Power Switch Frame
    //FrameLayout containing power btn
    private FrameLayout mPowerSwitchFrame;

    // ImageView used as UP/DOWN sliding switch for power on/off selection
    // used to handle slide events
    // as well as click events
    private ImageView mPowerToggleImgView;

    // reference to power switch Y displacement during touch events
    // to calculate toggling point
    private int mPowerSwitchYDelta = 0;

    // reference to power switch current Y position
    private int mCurrentYPos = 0;

    // reference to power switch X,Y position
    private Point mPowerSwitchXY = new Point(0,200);

    //endregion

    //region ## Display Mode Frame

    //FrameLayout containing flash mode btn
    private FrameLayout mDisplayModeFrame;

    // ImageView used as LEFT/RIGHT sliding switch for flash mode selection
    // used to handle slide events
    // as well as click events
    private ImageView mDisplayModeImgView;

    // reference to display mode switch X displacement during touch events
    // to calculate toggling point
    private int mDisplayModeSwitchXDelta = 0;

    // reference to display mode switch current X value
    private int mDisplayModeSwitchXCurrent = 0;

    // reference to display mode switch X,Y position
    private Point mDisplayModeSwitchXY = new Point(0,140);
    //endregion

    //// View.OnLayoutChangeListener callback
    @Override
    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {

        if (mDisplayModeSwitchXY.equals(0,0) && mPowerSwitchXY.equals(0,0)) {
            RelativeLayout modeRelative = (RelativeLayout) findViewById(R.id.activity_main_screen_thumb_container);

            //only handle clicks if camera is supported, else only screen mode is an option
            if (modeRelative != null && mCameraSupported) {
                modeRelative.setOnClickListener(setupOnModeClickListener());
            }

            RelativeLayout powerRelative = (RelativeLayout) findViewById(R.id.activity_main_screen_power_thumb_container);
            if(powerRelative != null) {
                powerRelative.setOnClickListener(setupOnPowerClickListener());
            }

            try {
                //set initial display mode switch coordinates
                mDisplayModeSwitchXY.set(0,106);

                //set initial power switch coordinates
                mPowerSwitchXY.set(0,155);

                if(mDensityFactor > 2) {  // 2 -> xhdpi
                    mDisplayModeSwitchXY.y = (int) (mDisplayModeSwitchXY.y / 2 * mDensityFactor);
                    mPowerSwitchXY.y = (int) (mPowerSwitchXY.x / 2 * mDensityFactor);
                }

                updateImageViewLayoutParams(mDisplayModeImgView, mDisplayModeSwitchXY, mAppPreferences.isLedCameraSelected(), false);
                updateImageViewLayoutParams(mPowerToggleImgView, mPowerSwitchXY, mAppPreferences.isPowerOn(), true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //region ##LIFECYCLE METHODS

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCameraSupported = HardwareUtil.hasCompatibleCamera(MainActivity.this);
        mAppPreferences = AppPreferences.getInstance(this);
        mAppPreferences.setCameraHardwareAvailable(mCameraSupported);
        mDensityFactor = getResources().getDisplayMetrics().density;

        initUIComponents();

        // check if user has granted camera access permission
        if(Build.VERSION.SDK_INT >= 23 && !PermissionUtil.checkPermission(this, Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CAMERA);
        } else {
            handleAppFlow();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        setWidgetVisible(true);
        if (mAppPreferences.isPowerOn()) {
            NotificationUtil.createFlashlightNotification(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        NotificationUtil.cancelNotification(this);
        initializeControllers();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (PermissionUtil.permissionReslultOk(grantResults)) {
            handleAppFlow();
        }else {
            Toast.makeText(MainActivity.this, getString(R.string.toast_permission_denied), Toast.LENGTH_LONG).show();
        }
    }

    //endregion

    private void initUIComponents(){
        mDisplayModeImgView = (ImageView) findViewById(R.id.activity_main_screen_mode_thumb_img);
        mPowerToggleImgView = (ImageView) findViewById(R.id.activity_main_screen_power_thumb_img);
        mPowerSwitchFrame = (FrameLayout) findViewById(R.id.activity_main_screen_power_frame);
        mDisplayModeFrame = (FrameLayout) findViewById(R.id.activity_main_screen_mode_frame);
        mModeRadioGroup = (RadioGroup) findViewById(R.id.activity_main_radio_group);
        mMorseMsgEditTxt = (EditText) findViewById(R.id.activity_main_morse_edit_txt);
        mMorseMsgEditTxt.setText(AppPreferences.getInstance(MainActivity.this).getMorseModeMessage());

        mLinearLayout = (ViewGroup) findViewById(android.R.id.content);
        if(mLinearLayout != null){
            mLinearLayout.addOnLayoutChangeListener(this);
        }
    }

    private void handleAppFlow() {
        if (mCameraSupported) {
            //Initialize FlashLightService
            Intent intent = new Intent(MainActivity.this, FlashLightService.class);
            startService(intent);
        } else if(mAppPreferences.isFirstTime()){
            // If it's the app's first launch and camera is not supported, show an appropriate dialog
            AlertUtil.showNoCameraDialog(MainActivity.this);
            mAppPreferences.setFirstTime(false);
        }
    }

    private void setWidgetVisible(boolean visible){
        int visibility = visible ? View.VISIBLE : View.INVISIBLE;

        Intent intent = new Intent(this, FlashLightService.class);
        intent.setAction(FlashLightService.UPDATE_WIDGET);
        intent.putExtra(FlashLightService.WIDGET_VISIBILITY, visibility);
        startService(intent);
    }

    /**
     *
     * @param imageView - The imageview that needs to be altered
     * @param pos - The new xy coordinates corresponding to the view , according to which margins will be set
     * @param setOn - If should slide all the way to 'ON' position, which for led switch means left and power switch top
     * @param vertical - true if power switch, false if display mode switch
     */
    private void updateImageViewLayoutParams(ImageView imageView, Point pos, boolean setOn, boolean vertical){
        RelativeLayout.LayoutParams modeParams = (RelativeLayout.LayoutParams) imageView.getLayoutParams();

        if(vertical){
            modeParams.topMargin = setOn ? 0 : pos.y;
        } else {
            modeParams.leftMargin = setOn ? pos.x : 0;
        }
    }

    private void initializeControllers() {
        mModeRadioGroup.check(mAppPreferences.getSelectedFlashModeViewId());
        mModeRadioGroup.setOnCheckedChangeListener(this);

        //init led drawable for display mode switch according to selected mode saved in preferences
        setLedDrawables(mDisplayModeFrame,
                mAppPreferences.isLedCameraSelected(),
                R.drawable.img_lightselect_background_led,
                R.drawable.img_lightselect_background_screen);

        //init led drawable for power switch according to power state saved in preferences
        setLedDrawables(mPowerSwitchFrame,
                mAppPreferences.isPowerOn(),
                R.drawable.img_mainswitch_large_background_on,
                R.drawable.img_mainswitch_large_background_off);

        if (mCameraSupported) {
            setModeSliderTouchListener();
        }

        setPowerSliderTouchListener();
        updateImageViewLayoutParams(mPowerToggleImgView, mPowerSwitchXY, mAppPreferences.isPowerOn(), true);
        RelativeLayout.LayoutParams generalParams = (RelativeLayout.LayoutParams) mPowerToggleImgView.getLayoutParams();
        mPowerToggleImgView.setLayoutParams(generalParams);
    }

    /**
     * Power switch has a led on top indicating power on state
     * Display mode switch has a led on each side indicating selected mode state
     *
     * @param frame - The frame on which the background will be set
     * @param setOn - true if should set power 'ON' for power switch or 'LED' mode for display mode switch
     * @param onDrawableId - drawable id for 'ON' or 'LED' state
     * @param offDrawableId - drawable id for 'OFF' or 'SCREEN' state
     */
    private void setLedDrawables(FrameLayout frame, boolean setOn, int onDrawableId, int offDrawableId){
        if (setOn) {
            frame.setBackground(ContextCompat.getDrawable(MainActivity.this, onDrawableId));
        } else {
            frame.setBackground(ContextCompat.getDrawable(MainActivity.this, offDrawableId));
        }
    }

    private void setModeSliderTouchListener() {

        mDisplayModeImgView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                RelativeLayout.LayoutParams generalParams = (RelativeLayout.LayoutParams) mDisplayModeImgView.getLayoutParams();

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        mDisplayModeSwitchXDelta = (int) event.getRawX() - generalParams.leftMargin;
                        break;
                    case MotionEvent.ACTION_UP:
                        int modeBarDivider = mDisplayModeSwitchXY.y / 2;
                        if (mDisplayModeSwitchXCurrent <= modeBarDivider) {
                            generalParams.leftMargin = 0;
                            mAppPreferences.setLedCameraSelected(false);
                            mDisplayModeFrame.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.img_lightselect_background_screen));
                        } else {
                            generalParams.leftMargin = mDisplayModeSwitchXY.y;
                            mAppPreferences.setLedCameraSelected(true);
                            mDisplayModeFrame.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.img_lightselect_background_led));
                        }

                        mDisplayModeImgView.setLayoutParams(generalParams);
                        startFlashLight(mAppPreferences.getSelectedFlashMode());
                        break;
                    case MotionEvent.ACTION_MOVE:
                        generalParams.leftMargin = (int) event.getRawX() - mDisplayModeSwitchXDelta;
                        mDisplayModeSwitchXCurrent = (int) event.getRawX() - mDisplayModeSwitchXDelta;

                        if (mDisplayModeSwitchXCurrent < 0) {
                            generalParams.leftMargin = 0;
                        } else if (mDisplayModeSwitchXCurrent > mDisplayModeSwitchXY.y) {
                            generalParams.leftMargin = mDisplayModeSwitchXY.y;
                        }

                        mDisplayModeImgView.setLayoutParams(generalParams);
                        break;

                }
                mLinearLayout.invalidate();
                return true;
            }
        });
    }

    private void setPowerSliderTouchListener() {

        mPowerToggleImgView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                    final int Y = (int) event.getRawY();
                    RelativeLayout.LayoutParams generalParams = (RelativeLayout.LayoutParams) mPowerToggleImgView.getLayoutParams();
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            mPowerSwitchYDelta = Y - generalParams.topMargin;
                            mPowerOnActionDown = mAppPreferences.isPowerOn();
                            break;
                        case MotionEvent.ACTION_UP:
                            if(mAppPreferences.isPowerOn() != mPowerOnActionDown){
                                break;
                            }
                            int powerBarDivider = mPowerSwitchXY.y / 2;
                            if (mCurrentYPos <= powerBarDivider) {
                                generalParams.topMargin = 0;
                                mPowerSwitchFrame.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.img_mainswitch_large_background_on));
                                mAppPreferences.setPowerOn(true);
                                startFlashLight(mAppPreferences.getSelectedFlashMode());
                            } else {
                                generalParams.topMargin = mPowerSwitchXY.y;
                                mPowerSwitchFrame.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.img_mainswitch_large_background_off));
                                stopFlashLight();
                            }
                            mPowerToggleImgView.setLayoutParams(generalParams);
                            break;
                        case MotionEvent.ACTION_MOVE:
                            if(mAppPreferences.isPowerOn() != mPowerOnActionDown){
                                break;
                            }
                            generalParams.topMargin = Y - mPowerSwitchYDelta;
                            mCurrentYPos = Y - mPowerSwitchYDelta;

                            if (mCurrentYPos < 0) {
                                generalParams.topMargin = 0;
                                mPowerSwitchFrame.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.img_mainswitch_large_background_on));
                                mAppPreferences.setPowerOn(true);
                                startFlashLight(mAppPreferences.getSelectedFlashMode());
                            } else if (mCurrentYPos > mPowerSwitchXY.y ) {
                                generalParams.topMargin = mPowerSwitchXY.y;
                                mPowerSwitchFrame.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.img_mainswitch_large_background_off));
                                stopFlashLight();
                            }

                            mPowerToggleImgView.setLayoutParams(generalParams);
                            break;

                    }
                    mLinearLayout.invalidate();
                    return true;
                }
            });
    }

    private void startFlashLight(String flashMode) {
        startKnobSound(R.raw.light_on);

        //set current morse code message
        mAppPreferences.setMorseModeMessage(MorseCodeUtil.validateMorseMessage(mMorseMsgEditTxt.getText().toString()));

        if(AppPreferences.getInstance(this).isPowerOn()) {

            Intent serviceIntent = new Intent(this, FlashLightService.class);
            serviceIntent.setAction(flashMode);
            startService(serviceIntent);
        }
    }

    private void stopFlashLight() {
        startKnobSound(R.raw.light_off);

        if (mAppPreferences.isPowerOn()) {
            mAppPreferences.setPowerOn(false);
            Intent serviceIntent = new Intent(this, FlashLightService.class);
            serviceIntent.setAction(FlashLightService.MODE_OFF);
            startService(serviceIntent);
        }
    }

    /**
     * Play a sound when flash light is turned ON/OFF
     * @param playResource - the audio file to be played
     */
    private void startKnobSound(int playResource) {
        try {
            if (mMediaPlayer != null) {
                mMediaPlayer.stop();
                mMediaPlayer.reset();
                mMediaPlayer.release();
                mMediaPlayer = null;
            }
            mMediaPlayer = MediaPlayer.create(MainActivity.this, playResource);
            mMediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //// RadioGroup.OnCheckedChangeListener callback
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        mAppPreferences.setSelectedFlashModeViewId(checkedId);
        startKnobSound(R.raw.light_strobe_sos);

        @FlashLightService.ServiceAction String flashMode = "";
        switch (checkedId) {
            case R.id.activity_main_radio_flashlight_btn:
                group.check(R.id.activity_main_radio_flashlight_btn);
                mAppPreferences.setSelectedFlashMode(FlashLightService.MODE_CONTINUOUS);
                flashMode = FlashLightService.MODE_CONTINUOUS;
                break;
            case R.id.activity_main_radio_strobe_btn:
                group.check(R.id.activity_main_radio_strobe_btn);
                mAppPreferences.setSelectedFlashMode(FlashLightService.MODE_STROBE);
                flashMode = FlashLightService.MODE_STROBE;
                break;
            case R.id.activity_main_radio_sos_btn:
                group.check(R.id.activity_main_radio_sos_btn);
                mAppPreferences.setSelectedFlashMode(FlashLightService.MODE_MORSE);
                mAppPreferences.setMorseModeMessage(MorseCodeUtil.validateMorseMessage(mMorseMsgEditTxt.getText().toString()));
                flashMode = FlashLightService.MODE_MORSE;
                break;
        }

        startFlashLight(flashMode);
    }

    private View.OnClickListener setupOnPowerClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RelativeLayout.LayoutParams powerParams = (RelativeLayout.LayoutParams) mPowerToggleImgView.getLayoutParams();
                if (mAppPreferences.isPowerOn()) {
                    mPowerSwitchFrame.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.img_mainswitch_large_background_off));
                    powerParams.topMargin = mPowerSwitchXY.y;
                    mPowerToggleImgView.setLayoutParams(powerParams);
                    stopFlashLight();
                } else {
                    mPowerSwitchFrame.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.img_mainswitch_large_background_on));
                    powerParams.topMargin = 0;
                    mPowerToggleImgView.setLayoutParams(powerParams);
                    mAppPreferences.setPowerOn(true);
                    startFlashLight(mAppPreferences.getSelectedFlashMode());
                }
            }
        };
    }

    private View.OnClickListener setupOnModeClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startKnobSound(R.raw.led_screen);

                RelativeLayout.LayoutParams powerParams = (RelativeLayout.LayoutParams) mDisplayModeImgView.getLayoutParams();
                boolean isModeLed = !mAppPreferences.isLedCameraSelected();
                if (isModeLed) {
                    mDisplayModeFrame.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.img_lightselect_background_led));
                    powerParams.leftMargin = mDisplayModeSwitchXY.y;
                } else {
                    mDisplayModeFrame.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.img_lightselect_background_screen));
                    powerParams.leftMargin = 0;
                }
                mDisplayModeImgView.setLayoutParams(powerParams);
                mAppPreferences.setLedCameraSelected(isModeLed);
                startFlashLight(mAppPreferences.getSelectedFlashMode());
            }
        };
    }
}
