package com.sample.flashlight.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.LinearLayout;

import com.sample.flashlight.R;
import com.sample.flashlight.application.AppPreferences;
import com.sample.flashlight.service.FlashLightService;
import com.sample.flashlight.util.MorseCodeTick;
import com.sample.flashlight.util.MorseCodeUtil;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScreenFlashActivity extends AppCompatActivity {

    //Variable to store mBrightness value
    private int mBrightness = 0;

    private AppPreferences mAppPreferences;
    private LinearLayout mScreen;
    private Handler mHandler = new Handler();
    private boolean mScreenBright = false;
    private Window mWindow;
    private LayoutParams mLayoutParams;

    private ScheduledExecutorService mMorseCodeTicksExecutor;

    Runnable mStrobeRunnable = new Runnable() {
        @Override
        public void run() {
            try {
                if (mScreenBright) {
                    mScreen.setBackgroundColor(ContextCompat.getColor(ScreenFlashActivity.this, android.R.color.black));
                } else {
                    mScreen.setBackgroundColor(ContextCompat.getColor(ScreenFlashActivity.this, android.R.color.white));
                }
                mScreenBright = !mScreenBright;
            } finally {
                mHandler.postDelayed(this, 200);
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_flash);

        mAppPreferences = AppPreferences.getInstance(ScreenFlashActivity.this);
        mScreen = (LinearLayout) findViewById(R.id.activity_screen_flash_container);
        mScreen.setOnClickListener(setupOnWhiteScreenClicked());

        String action = getIntent().getAction();
        if(action != null && !action.isEmpty()){
            screenFlashOperation(action);
        }

        //Get the current Window
        mWindow = getWindow();

        try {
            //Get the current system mBrightness
            mBrightness = System.getInt(getContentResolver(), System.SCREEN_BRIGHTNESS);
        } catch (SettingNotFoundException e) {
            Log.e("Error", "Cannot access system Brightness");
            e.printStackTrace();
        }

        //Get the current mWindow attributes
        mLayoutParams = mWindow.getAttributes();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // set maximum mBrightness
        updateWindowBrightness(255);
        updateWidget(true);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // restore mBrightness on initial value
        updateWindowBrightness(mBrightness);
        updateWidget(false);
    }

    private void updateWindowBrightness(int value) {
        //Set the mBrightness of this mWindow
        mLayoutParams.screenBrightness = value / (float)255;
        //Apply attribute changes to this mWindow
        mWindow.setAttributes(mLayoutParams);
    }

    private View.OnClickListener setupOnWhiteScreenClicked() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // remove-close strobe operation
                mHandler.removeCallbacks(mStrobeRunnable);

                // remove-close morse operation
                if(mMorseCodeTicksExecutor != null && !mMorseCodeTicksExecutor.isShutdown()) {
                    mMorseCodeTicksExecutor.shutdownNow();
                }

                // switch off power btn and set on off position
                updateWidget(false);
                finish();
            }
        };
    }

    private void updateWidget(boolean powerOn){
        mAppPreferences.setPowerOn(powerOn);
        Intent intent = new Intent(ScreenFlashActivity.this, FlashLightService.class);
        intent.setAction(FlashLightService.UPDATE_WIDGET);
        startService(intent);
    }

    private void screenFlashOperation(String action) {
        switch (action) {
            case FlashLightService.MODE_OFF:
                finish();
                break;
            case FlashLightService.MODE_CONTINUOUS:
                // no need for further actions
                mScreen.setBackgroundColor(ContextCompat.getColor(ScreenFlashActivity.this, android.R.color.white));
                break;
            case FlashLightService.MODE_STROBE:
                // strobe operation
                strobeOperation();
                break;
            case FlashLightService.MODE_MORSE:
                // sos operation
                sosOperation();
                break;
        }
    }

    private void strobeOperation() {
        mHandler.postDelayed(mStrobeRunnable, 200);
    }

    private void sosOperation() {
        long delay = 500;
        ArrayList<MorseCodeTick> morseCodeTickArrayList = MorseCodeUtil.getMorseCodeSequence(mAppPreferences.getMorseModeMessage());

        if(morseCodeTickArrayList.size() == 0){
            return;
        }

        mMorseCodeTicksExecutor = Executors.newSingleThreadScheduledExecutor();


        for(int i=0;i<morseCodeTickArrayList.size();i++){
            final MorseCodeTick morseCodeTick = morseCodeTickArrayList.get(i);

            mMorseCodeTicksExecutor.schedule(
                    new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(morseCodeTick.isSignalOn()){
                                        mScreen.setBackgroundColor(ContextCompat.getColor(ScreenFlashActivity.this, android.R.color.white));
                                    } else {
                                        mScreen.setBackgroundColor(ContextCompat.getColor(ScreenFlashActivity.this, android.R.color.black));
                                    }
                                }
                            });
                        }
                    },
                    delay,
                    TimeUnit.MILLISECONDS);

            delay += morseCodeTick.getDuration();
        }
    }
}
