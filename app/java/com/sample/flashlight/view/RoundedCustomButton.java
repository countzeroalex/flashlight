package com.sample.flashlight.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.Button;

import com.sample.flashlight.R;


public class RoundedCustomButton extends Button {

    private static final float DEFAULT_TEXT_SIZE = 9;
    private float mRadius;
    private int mBackgroundColor;
    private String mText;
    private int mTextColor;
    private float mTextSize;
    private boolean mHasDrawableLeft;
    private int mDrawableLeft;

    public RoundedCustomButton(Context context) {
        super(context);
        initView();
    }

    public RoundedCustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.RoundedCustomButton, 0, 0);
        try {
            mRadius = typedArray.getFloat(R.styleable.RoundedCustomButton_radius, 5);
            mBackgroundColor = typedArray.getColor(R.styleable.RoundedCustomButton_backgroundColor, ContextCompat.getColor(context, R.color.colorPrimary));
            mText = typedArray.getString(R.styleable.RoundedCustomButton_text);
            mTextColor = typedArray.getColor(R.styleable.RoundedCustomButton_textColor, ContextCompat.getColor(context, android.R.color.white));
            mTextSize = typedArray.getFloat(R.styleable.RoundedCustomButton_textSize, DEFAULT_TEXT_SIZE);
            mDrawableLeft = typedArray.getResourceId(R.styleable.RoundedCustomButton_drawable_left, R.drawable.ic_stat_notification);
            mHasDrawableLeft = typedArray.getBoolean(R.styleable.RoundedCustomButton_has_drawable, false);
        } finally {
            typedArray.recycle();
        }
        initView();
    }

    public RoundedCustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        GradientDrawable shape = new GradientDrawable();
        shape.setCornerRadius(mRadius);
        shape.setColor(mBackgroundColor);
        setText(mText);
        setTextColor(mTextColor);
        setTextSize(mTextSize);
        setBackground(shape);
        setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

        if (mHasDrawableLeft) {
            setCompoundDrawablesWithIntrinsicBounds(mDrawableLeft, 0, 0, 0);
        }
    }
}
