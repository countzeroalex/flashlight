package com.sample.flashlight.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.sample.flashlight.R;
import com.sample.flashlight.application.AppPreferences;
import com.sample.flashlight.service.FlashLightService;

public class WidgetProvider extends AppWidgetProvider {

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        AppPreferences.getInstance(context).setFirstWidgetCall(true);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,int[] appWidgetIds) {

        AppPreferences preferences = AppPreferences.getInstance(context);

        // Get all ids
        ComponentName thisWidget = new ComponentName(context, WidgetProvider.class);
        int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
        for (int widgetId : allWidgetIds) {
            // create some random data
            RemoteViews remoteViews = new RemoteViews(
                    context.getPackageName(),
                    R.layout.widget_layout);

            //get flash state from preferences
            boolean flashOn = preferences.isPowerOn();

            //set flash enabled "light" view
            remoteViews.setImageViewResource(R.id.widget_enabled_light, flashOn ? R.drawable.widget_off :R.drawable.widget_on);

            // Register an onClickListener
            Intent intent = new Intent(context, WidgetProvider.class);

            intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    context,
                    0,
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            remoteViews.setOnClickPendingIntent(R.id.widget_enabled_light, pendingIntent);
            appWidgetManager.updateAppWidget(widgetId, remoteViews);

            if(preferences.isFirstWidgetCall()){
                //don't start flash on first call, since the widget was just added to the screen,
                //so the call is from dragging it to position
                preferences.setFirstWidgetCall(false);
                remoteViews.setImageViewResource(R.id.widget_enabled_light, R.drawable.widget_off);
                return;
            } else {
                //send intent to FlashLightService to handle flash functionality
                Intent toggleFlashIntent = new Intent(context, FlashLightService.class);
                if(flashOn) {
                    toggleFlashIntent.setAction(FlashLightService.MODE_OFF);
                } else {
                    toggleFlashIntent.setAction(preferences.getSelectedFlashMode());
                }

                toggleFlashIntent.putExtra(FlashLightService.FROM_WIDGET, true);
                context.startService(toggleFlashIntent);
            }
        }
    }
}