package com.sample.flashlight.util;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.sample.flashlight.R;
import com.sample.flashlight.application.AppPreferences;
import com.sample.flashlight.service.FlashLightService;

public class NotificationUtil {

    private static final int FLASHLIGHT_NOTIFICATION_ID = 555;
    private static final int CAMERA_ERROR_NOTIFICATION_ID = 556;

    public static void createFlashlightNotification(Context context) {
        if(!AppPreferences.getInstance(context).isLedCameraSelected() || !AppPreferences.getInstance(context).isCameraHardwareAvailable()){
            return;
        }

        Intent serviceAction = new Intent(context, FlashLightService.class);
        serviceAction.setAction(FlashLightService.MODE_OFF);
        PendingIntent contentIntent = PendingIntent.getService(
                context,
                FLASHLIGHT_NOTIFICATION_ID,
                serviceAction,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_stat_notification)
                        .setContentTitle(context.getResources().getString(R.string.app_name))
                        .setAutoCancel(true)
                        .setSound(null)
                        .setContentText(context.getString(R.string.flashlight_notification_text))
                        .setTicker(context.getString(R.string.flashlight_notification_text))
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(context.getString(R.string.flashlight_notification_text)))
                        .setPriority(android.app.Notification.PRIORITY_MAX);

        if (contentIntent != null) {
            mBuilder.setContentIntent(contentIntent);
        }

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(FLASHLIGHT_NOTIFICATION_ID, mBuilder.build());
    }

    public static void cancelNotification(Context context){
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(FLASHLIGHT_NOTIFICATION_ID);
    }

    public static void createCameraErrorNotification(Context context){
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(context)
                            .setSmallIcon(R.drawable.ic_stat_notification)
                            .setContentTitle(context.getResources().getString(R.string.app_name))
                            .setAutoCancel(true)
                            .setSound(null)
                            .setContentText(context.getString(R.string.camera_error_dialog_message))
                            .setTicker(context.getString(R.string.camera_error_dialog_message))
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(context.getString(R.string.camera_error_dialog_message)))
                            .setPriority(android.app.Notification.PRIORITY_MAX);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(CAMERA_ERROR_NOTIFICATION_ID, mBuilder.build());
    }
}
