package com.sample.flashlight.util;

public class MorseCodeTick {

    /**
     * Morse Code Guidelines
     *
     * 1.The length of a dot is one unit
     * 2.A dash is three units
     * 3.The space between parts of the same letter is one unit.
     * 4.The space between letters is three units
     * 5.The space between words is seven units
     */

    public static final char LETTER_PART_SPACE = 'p';
    public static final char LETTER_SPACE = 'l';
    public static final char WORD_SPACE = 'w';

    private static final Long UNIT_DURATION_IN_MILLIS = 500L;
    private static final Long DOT = UNIT_DURATION_IN_MILLIS;//signal on
    private static final Long DASH = 3*UNIT_DURATION_IN_MILLIS;//signal on
    private static final Long SPACE_BETWEEN_LETTER_PARTS = UNIT_DURATION_IN_MILLIS;//signal off
    private static final Long SPACE_BETWEEN_LETTERS = 3*UNIT_DURATION_IN_MILLIS;//signal off
    private static final Long SPACE_BETWEEN_WORDS = 7*UNIT_DURATION_IN_MILLIS;//signal off

    protected boolean mSignalOn;
    protected long mDuration;

    public MorseCodeTick(char c){
        switch (c){
            case '.':
                setSignalOn(true);
                setDuration(DOT);
                break;
            case '-':
                setSignalOn(true);
                setDuration(DASH);
                break;
            case LETTER_PART_SPACE:
                setSignalOn(false);
                setDuration(SPACE_BETWEEN_LETTER_PARTS);
                break;
            case LETTER_SPACE:
                setSignalOn(false);
                setDuration(SPACE_BETWEEN_LETTERS);
                break;
            case WORD_SPACE:
                setSignalOn(false);
                setDuration(SPACE_BETWEEN_WORDS);
                break;
        }
    }

    public boolean isSignalOn() {
        return mSignalOn;
    }

    public void setSignalOn(boolean signalOn) {
        mSignalOn = signalOn;
    }

    public long getDuration() {
        return mDuration;
    }

    public void setDuration(long duration) {
        mDuration = duration;
    }
}
