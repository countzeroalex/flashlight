package com.sample.flashlight.util;

import java.util.ArrayList;
import java.util.TreeMap;

public class MorseCodeUtil {

    private static final TreeMap<Character, char[]> morseChars;
    static{
        /**
         * DOT - '.'
         * DASH - '-'
         * SPACE_BETWEEN_LETTER_PARTS - 'p'
         * SPACE_BETWEEN_WORDS - 'w'
         * SPACE_BETWEEN_LETTERS - 'l'
         */
        morseChars = new TreeMap<>();
        morseChars.put('A',new char[]{'.','p','-'});
        morseChars.put('B',new char[]{'-','p','.','p','.','p','.'});
        morseChars.put('C',new char[]{'-','p','.','p','-','p','.'});
        morseChars.put('D',new char[]{'-','p','.','p','.'});
        morseChars.put('E',new char[]{'.'});
        morseChars.put('F',new char[]{'.','p','.','p','-','p','.'});
        morseChars.put('G',new char[]{'-','p','-','p','.'});
        morseChars.put('H',new char[]{'.','p','.','p','.','p','.'});
        morseChars.put('I',new char[]{'.','p','.'});
        morseChars.put('J',new char[]{'.','p','-','p','-','p','-'});
        morseChars.put('K',new char[]{'-','p','.','p','-','p','.'});
        morseChars.put('L',new char[]{'.','p','-','p','.','p','.'});
        morseChars.put('M',new char[]{'-','p','-'});
        morseChars.put('N',new char[]{'-','p','.'});
        morseChars.put('O',new char[]{'-','p','-','p','-'});
        morseChars.put('P',new char[]{'.','p','-','p','-','p','.'});
        morseChars.put('Q',new char[]{'-','p','-','p','.','p','-'});
        morseChars.put('R',new char[]{'.','p','-','p','.'});
        morseChars.put('S',new char[]{'.','p','.','p','.'});
        morseChars.put('T',new char[]{'-'});
        morseChars.put('U',new char[]{'.','p','.','p','-'});
        morseChars.put('V',new char[]{'.','p','.','p','.','p','-'});
        morseChars.put('W',new char[]{'.','p','-','p','-'});
        morseChars.put('X',new char[]{'-','p','.','p','.','p','-'});
        morseChars.put('Y',new char[]{'-','p','.','p','-','p','-'});
        morseChars.put('Z',new char[]{'-','p','-','p','.','p','.'});
        morseChars.put('1',new char[]{'.','p','-','p','-','p','-','p','-'});
        morseChars.put('2',new char[]{'.','p','.','p','-','p','-','p','-'});
        morseChars.put('3',new char[]{'.','p','.','p','.','p','-','p','-'});
        morseChars.put('4',new char[]{'.','p','.','p','.','p','.','p','-'});
        morseChars.put('5',new char[]{'.','p','.','p','.','p','.','p','.'});
        morseChars.put('6',new char[]{'-','p','.','p','.','p','.','p','.'});
        morseChars.put('7',new char[]{'-','p','-','p','.','p','.','p','.'});
        morseChars.put('8',new char[]{'-','p','-','p','-','p','.','p','.'});
        morseChars.put('9',new char[]{'-','p','-','p','-','p','-','p','.'});
        morseChars.put('0',new char[]{'-','p','-','p','-','p','-','p','-'});
        morseChars.put(' ',new char[]{'w'});
    }

    /**
     *
     * @param message - Message to be translated in Morse Code
     * @return - an ArrayList containing MorseCodeTick Objects
     */
    public static ArrayList<MorseCodeTick> getMorseCodeSequence(String message) {
        ArrayList<MorseCodeTick> morseCodeTickArrayList = new ArrayList<>();

        for (int i = 0; i < message.length(); i++) {
            char c = message.charAt(i);

            char[] morseChar = morseChars.get(c);
            for(int j=0;j<morseChar.length;j++){
                morseCodeTickArrayList.add(new MorseCodeTick(morseChar[j]));
            }

            if(c != ' ') {
                morseCodeTickArrayList.add(new MorseCodeTick('l'));
            }
        }
        return morseCodeTickArrayList;
    }

    /**
     *
     * @param message - User input to translate in morse code
     * @return - validated message, only alphabet characters and whitespace
     */
    public static String validateMorseMessage(String message){
        String validMessage = message.replaceAll("[^a-zA-Z 0-9]+", "");
        return validMessage.toUpperCase();
    }
}