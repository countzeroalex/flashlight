package com.sample.flashlight.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.sample.flashlight.R;

public class AlertUtil {

    public static void showNoCameraDialog(Context context) {
        new AlertDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.unsupported_hardware_dialog_title))
                .setMessage(context.getResources().getString(R.string.unsupported_hardware_dialog_message))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
