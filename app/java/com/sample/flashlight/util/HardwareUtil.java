package com.sample.flashlight.util;

import android.content.Context;
import android.content.pm.PackageManager;

public class HardwareUtil {

    /** Check if this device has a compatible camera */
    public static boolean hasCompatibleCamera(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)
                && context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }
}
